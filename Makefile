.PHONY: all README.md README.gmi

all: README.md README.gmi

README.md:
	lml build md README.lml README.md

README.gmi:
	lml build gmi README.lml README.gmi
