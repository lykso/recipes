# Recipes

I am working on a meal planner inspired by the paper Towards a software tool for general meal optimisation. I would have just used the code published along with that paper, but it would not run due to missing class definitions and changed or missing variable names. Trying to sort through the bugs seemed like less fun than just writing my own version, and would certainly have been less edifying as well.

In working on the initial prototype, I found that simply deriving nutrition data from the ingredients list of a recipe is insufficient. Ingredients may have different nutritional properties depending on whether they are raw, boiled, fried, baked, and so on. Ingredients may also be divided and processed differently in the course of executing the recipe. Finally, ingredient entries may have notes included which, though useful for the recipe, only get in the way of parsing out the amount and type of ingredient to look up. For this reason, I've decided to add a `nutrient_sources` list separate from each recipe's `ingredients` list to each recipe. The format of each item in the list is:

```
- ingredient: <name of entry in nutrition_data table>
  quantity: <amount>
  unit: <unit name>
```

While simply adding nutrition data thus derived to the recipes would have solved the immediate problem, this approach was chosen to allow easy auditing of said data.

The Go module in this repository will include an ncurses-based utility for expediting the process of adding these lists to the recipes. (I have not begun to write this piece yet, though.)

I've also included the shell script I wrote during the development of the prototype, which uses fzf to do the same thing, but in a more linear, limited fashion.

At present, this utility can analyze recipe YAML files and display their nutrition data.

 

=> https://www.researchgate.net/publication/362296037_Towards_a_software_tool_for_general_meal_optimisation The paper which inspired this effort

=> https://github.com/JamesAIzzard/PyDiet The code from the paper

# Example recipe YAML

This is the recipe I've been using to test my code during development.

```A YAML file describing the recipe for anise bread with sweet pear sauce from Grim Grains.
title: anise bread with sweet pear sauce
author: Hundred Rabbits (100r.co)
license: https://creativecommons.org/licenses/by-nc-sa/4.0/
image: images/anise_bread_with_sweet_pear_sauce.jpg
image_credit: Hundred Rabbits (100r.co)
image_license: https://creativecommons.org/licenses/by-nc-sa/4.0/

tags:
- vegan
- dessert

servings: 2

ingredients:
- flax seeds (7 g)
- water (45 ml)
- granulated sugar (50 g)
- anise seeds (5 g, ground)
- all purpose flour (90 g)
- baking powder (7 g)
- soy milk (150 ml)
- bosc pear (1)
- canola oil (60 ml)
- granulated sugar (15 g)
- maple syrup (15 ml)

directions:
- Preheat oven to 160°C (325°F).
- Put 7 g (1 tbsp) of ground flax seeds with 45 ml (3 tbsp) of water, let thicken for 5 minutes (this is your flax 'egg'). Mix in 50 g (1/4 cup) of sugar and stir until dissolved. Add 5 g (1 tsp) of ground anise seeds, and mix once more.
- Add 90 g (3/4 cup) of all purpose flour to a bowl with 7 ml (1 1/2 tsp) of baking powder. Mix.
- Pour the wet ingredients into the dry ones, while gradually adding a bit over 150 ml of soy milk.
- Transfer evenly, to 2 mini loaf pans.
- Cut your bosc pear into cubes.
- Heat a pan at medium to high heat, add 60 ml (1/4 cup) of canola oil. Once melted, add 15 g (1 tbsp) of sugar and 15 ml (1 tbsp) of maple syrup.
- Once it starts to boil, add the cubed bosc pear and lower the heat. Let it bathe in the sweet mixture for 3-5 minutes.
- Pour the sauce over the 2 mini loaves, bake for 30 minutes.
- Let cool and serve!

nutrient_sources:
- ingredient: Seeds, flaxseed
  quantity: 7
  unit: gram
- ingredient: Sugars, granulated
  quantity: 65
  unit: gram
- ingredient: Spices, anise seed
  quantity: 5
  unit: gram
- ingredient: Wheat flour, white, all-purpose, unenriched
  quantity: 90
  unit: gram
- ingredient: Leavening agents, baking powder, double-acting, straight phosphate
  quantity: 7
  unit: gram
- ingredient: Soymilk, original and vanilla, unfortified
  quantity: 5
  unit: fl oz
- ingredient: Pears, raw, bosc (Includes foods for USDA's Food Distribution Program)
  quantity: 1
  unit: large
- ingredient: Oil, canola
  quantity: 12
  unit: tsp
- ingredient: Syrups, maple
  quantity: 0.0625
  unit: cup
```

