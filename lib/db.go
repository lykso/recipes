package lib

import (
	"github.com/glebarez/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var db *gorm.DB

func DB(path string) (*gorm.DB, error) {
	if db == nil {
		var err error
		db, err = gorm.Open(sqlite.Open(path), &gorm.Config{
			PrepareStmt:          true,
			FullSaveAssociations: true,
			// TODO: Create a logger that sets the bottom part of the TUI frame text and/or
			// ignore warning messages about slow queries.
			Logger:               logger.Default.LogMode(logger.Silent),
		})

		if err != nil {
			return nil, err
		}
	}
	return db, nil
}
