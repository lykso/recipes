package lib

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"

	"codeberg.org/lykso/nutrition"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"gorm.io/gorm"
)

type NutrientInputsTUI struct {
	ListableForm
	IngredientInput *tview.InputField
	QuantityInput   *tview.InputField
	UnitInput       *tview.InputField
}

type TUI struct {
	Recipe            *Recipe
	FilePath          string
	App               *tview.Application
	Screen            tcell.Screen
	MainFlex          *tview.Flex
	LeftFlex          *tview.Flex
	RightFlex         *tview.Flex
	BasicsFlex        *tview.Flex
	TitleInput        *tview.InputField
	AuthorInput       *tview.InputField
	LicenseInput      *tview.InputField
	ServingsInput     *tview.InputField
	ImageFlex         *tview.Flex
	ImagePathInput    *tview.InputField
	ImageCreditInput  *tview.InputField
	ImageLicenseInput *tview.InputField
	TagsList          *InputList[*ListableForm]
	IngredientsList   *InputList[*ListableForm]
	DirectionsFlex    *tview.Flex
	DirectionsArea    *tview.TextArea
	NutrientsFormList *InputList[*NutrientInputsTUI]
	Main              *tview.Frame
	Pages             *tview.Pages
	db                *gorm.DB
}

const flexSize = 0
const fixedSize = 1

func (tui *TUI) NewNutrientInputsTUI() *NutrientInputsTUI {
	newNutrientInputsTUI := NutrientInputsTUI{}
	cleanRE := regexp.MustCompile("[\\(\\),]+")
	nsIngredientFn := func(prompt string) []string {
		if tui.db == nil || len(prompt) < 3 {
			return nil
		}

		var data []nutrition.NutritionDataAutocomplete
		result := db.Where("name match ?", cleanRE.ReplaceAllString(prompt, "")+"*").Order("bm25(nd_ac)").Limit(10).Find(&data)

		if result.Error != nil {
			log.Fatal(result.Error)
		}

		if result.RowsAffected == 0 {
			return nil
		}
		suggestions := make([]string, result.RowsAffected+1)

		for i := 0; i < len(data); i += 1 {
			suggestions[i] = data[i].Name
		}
		suggestions[len(data)] = ""

		return suggestions
	}

	nsUnitFn := func(prompt string) []string {
		ingredient := strings.TrimSpace(newNutrientInputsTUI.IngredientInput.GetText())
		if tui.db == nil || ingredient == "" {
			return nil
		}

		var data nutrition.NutritionData
		result := db.Where("name = ?", ingredient).First(&data)
		if result.Error != nil {
			log.Fatal(result.Error)
		}

		var units []nutrition.Unit
		result = db.Where("nutrition_data_id = ? and unit like ?", data.ID, prompt+"%").Find(&units)
		if result.RowsAffected == 0 {
			return nil
		}

		suggestions := make([]string, result.RowsAffected+1)

		for i := 0; i < len(units); i += 1 {
			suggestions[i] = units[i].Unit
		}
		suggestions[len(units)] = ""

		return suggestions
	}
	newNutrientInputsTUI.ListableForm = *NewListableForm()
	newNutrientInputsTUI.IngredientInput = tview.NewInputField().SetLabel("Ingredient").SetAutocompleteFunc(nsIngredientFn)
	newNutrientInputsTUI.QuantityInput = tview.NewInputField().SetLabel("Quantity")
	newNutrientInputsTUI.UnitInput = tview.NewInputField().SetLabel("Unit").SetAutocompleteFunc(nsUnitFn)

	newNutrientInputsTUI.SetHeight(5)
	newNutrientInputsTUI.Form.SetItemPadding(0)
	newNutrientInputsTUI.AddInputField(newNutrientInputsTUI.IngredientInput)
	newNutrientInputsTUI.AddInputField(newNutrientInputsTUI.QuantityInput)
	newNutrientInputsTUI.AddInputField(newNutrientInputsTUI.UnitInput)

	return &newNutrientInputsTUI
}

func NewListableSingleInputForm() *ListableForm {
	field := tview.NewInputField()
	form := NewListableForm()
	form.SetBorderPadding(0, 0, 0, 0)
	form.SetItemPadding(0)
	form.SetHeight(1)
	form.AddInputField(field)
	return form
}

func (tui *TUI) NewTagForm() *ListableForm {
	form := NewListableSingleInputForm()
	cleanRE := regexp.MustCompile("[\\(\\),]+")
	form.InputFields[0].SetAutocompleteFunc(func(prompt string) []string {
		if tui.db == nil || len(prompt) < 3 {
			return nil
		}

		var data []nutrition.TagAutocomplete
		result := db.Where("name match ?", cleanRE.ReplaceAllString(prompt, "")+"*").Order("bm25(nd_ac)").Limit(10).Find(&data)

		if result.Error != nil {
			log.Fatal(result.Error)
		}

		if result.RowsAffected == 0 {
			return nil
		}
		suggestions := make([]string, result.RowsAffected+1)

		for i := 0; i < len(data); i += 1 {
			suggestions[i] = data[i].Name
		}
		suggestions[len(data)] = ""

		return suggestions
	})

	return form
}

func NewTUI(db *gorm.DB) *TUI {
	tui := TUI{db: db}
	tui.App = tview.NewApplication()
	tui.Screen, _ = tcell.NewScreen()
	tui.App.SetScreen(tui.Screen)

	// TODO: update on redraws
	_, height := tui.Screen.Size()

	tui.MainFlex = tview.NewFlex()
	tui.MainFlex.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyCtrlS {
			err := tui.UpdateRecipe() // Update underlying recipe model to match form elements.
			if err != nil {
				return nil
			}

			if tui.db != nil {
				tui.Recipe.SaveToDB(db)
			}

			if tui.FilePath != "" {
				tui.Recipe.ToFile(tui.FilePath)
				tui.Main.Clear()
				tui.Main.AddText(fmt.Sprintf("Saved to %s", tui.FilePath), false, tview.AlignLeft, tcell.ColorWhite)
			} else {
				tui.Main.Clear()
				tui.Main.AddText("No filepath set! TBD.", false, tview.AlignLeft, tcell.ColorRed)
			}
			return nil

		} else if event.Modifiers() == tcell.ModAlt && event.Rune() == 'b' {
			tui.App.SetFocus(tui.BasicsFlex)
			return nil

		} else if event.Modifiers() == tcell.ModAlt && event.Rune() == 'i' {
			tui.App.SetFocus(tui.ImageFlex)
			return nil

		} else if event.Modifiers() == tcell.ModAlt && event.Rune() == 'd' {
			tui.App.SetFocus(tui.DirectionsFlex)
			return nil

		} else if event.Modifiers() == tcell.ModAlt && event.Rune() == 'n' {
			tui.IngredientsList.Focus()
			return nil

		} else if event.Modifiers() == tcell.ModAlt && event.Rune() == 's' {
			tui.NutrientsFormList.Focus()
			return nil

		} else if event.Modifiers() == tcell.ModAlt && event.Rune() == 't' {
			tui.TagsList.Focus()
			return nil
		}
		return event
	})

	tui.LeftFlex = tview.NewFlex().SetDirection(tview.FlexRow)
	tui.RightFlex = tview.NewFlex().SetDirection(tview.FlexRow)

	tui.Main = tview.NewFrame(tui.MainFlex).SetBorders(0, 0, 0, 0, 0, 0).AddText("", false, tview.AlignLeft, tcell.ColorBlack)
	tui.Pages = tview.NewPages()
	tui.Pages.AddPage("main", tui.Main, true, true)

	tui.DirectionsFlex = tview.NewFlex()
	tui.DirectionsFlex.SetBorder(true).SetTitle("Directions")
	tui.DirectionsArea = tview.NewTextArea()
	tui.DirectionsFlex.AddItem(tui.DirectionsArea, flexSize, 1, true)

	tui.NutrientsFormList = NewInputList(tui.App, tui.NewNutrientInputsTUI, height, 5)
	tui.NutrientsFormList.Grid.SetBorder(true).SetTitle("Nutrient Sources")

	tui.IngredientsList = NewInputList(tui.App, NewListableSingleInputForm, height, 1)
	tui.IngredientsList.Grid.SetBorder(true).SetTitle("Ingredients")

	tui.BasicsFlex = tview.NewFlex().SetDirection(tview.FlexRow)
	tui.BasicsFlex.SetBorder(true).SetTitle("Basics")

	tui.TitleInput = tview.NewInputField().SetLabel("   Title: ")
	tui.BasicsFlex.AddItem(tui.TitleInput, fixedSize, 1, true)

	tui.AuthorInput = tview.NewInputField().SetLabel("  Author: ")
	tui.BasicsFlex.AddItem(tui.AuthorInput, fixedSize, 1, true)

	tui.LicenseInput = tview.NewInputField().SetLabel(" License: ")
	tui.BasicsFlex.AddItem(tui.LicenseInput, fixedSize, 1, true)

	tui.ServingsInput = tview.NewInputField().SetLabel("Servings: ")
	tui.BasicsFlex.AddItem(tui.ServingsInput, fixedSize, 1, true)

	tui.ImageFlex = tview.NewFlex().SetDirection(tview.FlexRow)
	tui.ImageFlex.SetBorder(true).SetTitle("Image")

	tui.ImagePathInput = tview.NewInputField().SetLabel("    Path: ")
	tui.ImageFlex.AddItem(tui.ImagePathInput, fixedSize, 1, true)

	tui.ImageCreditInput = tview.NewInputField().SetLabel("  Credit: ")
	tui.ImageFlex.AddItem(tui.ImageCreditInput, fixedSize, 1, true)

	tui.ImageLicenseInput = tview.NewInputField().SetLabel(" License: ")
	tui.ImageFlex.AddItem(tui.ImageLicenseInput, fixedSize, 1, true)

	tui.TagsList = NewInputList(tui.App, NewListableSingleInputForm, height, 1)
	tui.TagsList.Grid.SetBorder(true).SetTitle("Tags")

	tui.LeftFlex.AddItem(tui.BasicsFlex, flexSize, 1, false)
	tui.LeftFlex.AddItem(tui.ImageFlex, flexSize, 1, false)
	tui.LeftFlex.AddItem(tui.TagsList.Grid, flexSize, 1, false)
	tui.LeftFlex.AddItem(tui.DirectionsFlex, flexSize, 3, false)

	tui.RightFlex.AddItem(tui.IngredientsList.Grid, flexSize, 1, false)
	tui.RightFlex.AddItem(tui.NutrientsFormList.Grid, flexSize, 1, false)

	tui.MainFlex.AddItem(tui.LeftFlex, flexSize, 1, false)
	tui.MainFlex.AddItem(tui.RightFlex, flexSize, 1, false)

	tui.App.SetRoot(tui.Pages, true).EnableMouse(true)

	return &tui
}

func (tui *TUI) LoadRecipe(recipe *Recipe) {
	tui.Recipe = recipe

	directions := ""
	for _, direction := range recipe.Directions {
		directions += fmt.Sprintf("%s\n\n", direction)
	}

	tui.DirectionsArea.SetText(directions, false)

	tui.TitleInput.SetText(recipe.Title)
	tui.AuthorInput.SetText(recipe.Author)
	tui.LicenseInput.SetText(recipe.License)
	tui.ServingsInput.SetText(fmt.Sprintf("%d", recipe.Servings))

	tui.ImagePathInput.SetText(recipe.Image)
	tui.ImageCreditInput.SetText(recipe.ImageCredit)
	tui.ImageLicenseInput.SetText(recipe.ImageLicense)

	for _, tag := range recipe.Tags {
		field := tview.NewInputField().SetText(tag)
		form := NewListableForm()
		form.SetBorderPadding(0, 0, 0, 0)
		form.SetItemPadding(0)
		form.SetHeight(1)
		form.AddInputField(field)
		tui.TagsList.AddChild(form)
	}

	for _, ingredient := range recipe.Ingredients {
		field := tview.NewInputField().SetText(ingredient)
		form := NewListableForm()
		form.SetBorderPadding(0, 0, 0, 0)
		form.SetItemPadding(0)
		form.SetHeight(1)
		form.AddInputField(field)
		tui.IngredientsList.AddChild(form)
	}
	if len(tui.IngredientsList.Children) == 0 {
		tui.IngredientsList.AddChild(tui.IngredientsList.NewFn())
	}

	//NutrientInputsTUI
	for _, source := range recipe.NutrientSources {
		nutrientsTUI := tui.NewNutrientInputsTUI()
		nutrientsTUI.IngredientInput.SetText(source.Ingredient)
		nutrientsTUI.QuantityInput.SetText(fmt.Sprintf("%.2f", source.Quantity))
		nutrientsTUI.UnitInput.SetText(source.Unit)

		tui.NutrientsFormList.AddChild(nutrientsTUI)
	}
	if len(tui.NutrientsFormList.Children) == 0 {
		tui.NutrientsFormList.AddChild(tui.NutrientsFormList.NewFn())
	}

	if tui.db != nil {
		tui.Recipe.SaveToDB(db)
	}
}

func (tui *TUI) UpdateRecipe() error {
	var directions []string
	for _, direction := range strings.Split(tui.DirectionsArea.GetText(), "\n\n") {
		directions = append(directions, direction)
	}

	tui.Recipe.Title = tui.TitleInput.GetText()
	tui.Recipe.Author = tui.AuthorInput.GetText()
	tui.Recipe.License = tui.LicenseInput.GetText()

	var err error
	tui.Recipe.Servings, err = strconv.ParseUint(tui.ServingsInput.GetText(), 10, 64)
	if err != nil {
		return err
	}

	tui.Recipe.Image = tui.ImagePathInput.GetText()
	tui.Recipe.ImageCredit = tui.ImageCreditInput.GetText()
	tui.Recipe.ImageLicense = tui.ImageLicenseInput.GetText()

	var tags []string
	for _, child := range tui.TagsList.Children {
		if child.Empty() {
			continue
		}
		tags = append(tags, strings.TrimSpace(child.InputFields[0].GetText()))
	}
	tui.Recipe.Tags = tags

	var ingredients []string
	for _, child := range tui.IngredientsList.Children {
		if child.Empty() {
			continue
		}
		ingredients = append(ingredients, strings.TrimSpace(child.InputFields[0].GetText()))
	}
	tui.Recipe.Ingredients = ingredients

	//NutrientInputsTUI
	var nutrientSources []NutrientSource

	for _, child := range tui.NutrientsFormList.Children {
		if child.Empty() {
			continue
		}
		source := NutrientSource{}
		source.Ingredient = child.IngredientInput.GetText()
		source.Unit = child.UnitInput.GetText()
		source.Quantity, err = strconv.ParseFloat(child.QuantityInput.GetText(), 64)
		if err != nil {
			return err
		}
		nutrientSources = append(nutrientSources, source)
	}
	tui.Recipe.NutrientSources = nutrientSources

	return nil
}

func tuiModal(p tview.Primitive, width, height int) tview.Primitive {
	return tview.NewFlex().
		AddItem(nil, flexSize, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(nil, flexSize, 1, false).
			AddItem(p, height, 1, true).
			AddItem(nil, flexSize, 1, false), width, 1, true).
		AddItem(nil, flexSize, 1, false)
}
