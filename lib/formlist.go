package lib

import (
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type Listable interface {
	Empty() bool
	SetFocusFn(func()) Listable
	Height() int
	SetHeight(int) Listable
	Primitive() tview.Primitive
}

type ListableForm struct {
	*tview.Form
	InputFields []*tview.InputField

	/* Getting the height from GetRect seems busted right now, so I'm
	managing this manually. Even rendering to a tcell.SimulationScreen
	before trying to get the height doesn't seem to work. */
	height int
}

type ListableInputField struct {
	*tview.InputField
	height int
}

type InputList[T Listable] struct {
	App          *tview.Application
	Children     []T
	FocusIndex   int
	Grid         *tview.Grid
	MaxHeight    int
	MinRowHeight int
	NewFn        func() T
}

func NewListableInputField() *ListableInputField {
	return &ListableInputField{InputField: tview.NewInputField(), height: -1}
}

func (field *ListableInputField) Empty() bool {
	return strings.TrimSpace(field.GetText()) == ""
}

func (field *ListableInputField) Height() int {
	if field.height == -1 {
		_, _, _, height := field.GetInnerRect()
		return height
	}
	return field.height
}

func (field *ListableInputField) SetFocusFn(fn func()) Listable {
	field.SetFocusFunc(fn)
	return field
}

func (field *ListableInputField) SetText(text string) *ListableInputField {
	field.InputField.SetText(text)
	return field
}

func (field *ListableInputField) SetHeight(height int) Listable {
	field.height = height
	return field
}

func (field *ListableInputField) Primitive() tview.Primitive {
	return field
}

func NewListableForm() *ListableForm {
	form := &ListableForm{Form: tview.NewForm(), height: -1}
	return form
}

func (form *ListableForm) Height() int {
	if form.height == -1 {
		_, _, _, height := form.GetInnerRect()
		return height
	}
	return form.height
}

func (form *ListableForm) SetFocusFn(fn func()) Listable {
	for _, field := range form.InputFields {
		field.SetFocusFunc(fn)
	}
	return form
}

func (form *ListableForm) SetHeight(height int) Listable {
	form.height = height
	return form
}

func (form *ListableForm) Primitive() tview.Primitive {
	return form
}

func (form *ListableForm) AddInputField(field *tview.InputField) {
	form.AddFormItem(field)
	form.InputFields = append(form.InputFields, field)
}

func (form *ListableForm) Empty() bool {
	for _, field := range form.InputFields {
		if strings.TrimSpace(field.GetText()) != "" {
			return false
		}
	}
	return true
}

func NewInputList[T Listable](app *tview.Application, newFn func() T, maxHeight, minRowHeight int) *InputList[T] {
	list := InputList[T]{App: app, NewFn: newFn, MaxHeight: maxHeight, MinRowHeight: minRowHeight}
	list.Grid = tview.NewGrid().SetColumns(1).SetGap(0, 0)
	list.Grid.SetInputCapture(list.GridInputCaptureFn)

	return &list
}

func (list *InputList[T]) Focus() {
	if len(list.Children) == 0 {
		return
	}
	list.App.SetFocus(list.Children[list.FocusIndex].Primitive())
}

func (list *InputList[T]) GridInputCaptureFn(event *tcell.EventKey) *tcell.EventKey {
	// Delete
	if event.Modifiers() == tcell.ModAlt && event.Rune() == 'x' && len(list.Children) > 0 {
		newChildren := make([]T, len(list.Children)-1)
		j := 0
		for i, child := range list.Children {
			if i == list.FocusIndex {
				continue
			}
			newChildren[j] = child
			j += 1
		}
		list.Children = newChildren
		list.RerenderGrid()

		if len(list.Children) > 0 {
			list.SetFocus(list.FocusIndex)
		}
		return nil
	}

	// Insert new
	if event.Modifiers() == tcell.ModAlt && (event.Rune() == 'O' || event.Rune() == 'o') {
		targetIndex := list.FocusIndex
		if event.Rune() == 'o' && len(list.Children) > 0 {
			targetIndex += 1
		}

		var currChild T
		newChild := list.NewFn()

		for i := targetIndex; i < len(list.Children); i++ {
			currChild = list.Children[i]
			list.Children[i] = newChild
			newChild = currChild
		}
		list.Children = append(list.Children, newChild)
		list.RerenderGrid()
		list.SetFocus(targetIndex)
		return nil
	}

	if len(list.Children) == 0 {
		return event
	}

	if event.Key() == tcell.KeyPgDn || event.Key() == tcell.KeyPgUp {
		delta := 1
		if event.Key() == tcell.KeyPgUp {
			delta = -1
		}

		var topOfLastPage int
		y, x := list.Grid.GetOffset()
		_, _, _, height := list.Grid.GetInnerRect()

		for i := len(list.Children) - 1; i >= 0 && height > 0; i-- {
			childHeight := list.Children[i].Height()

			if height <= childHeight {
				topOfLastPage = i
				break
			}
			height -= childHeight
		}

		if list.FocusIndex+delta < len(list.Children) && list.FocusIndex+delta >= 0 {
			list.SetFocus(list.FocusIndex + delta)
		}

		if y+delta <= topOfLastPage && y+delta >= 0 {
			list.Grid.SetOffset(y+delta, x)
		}
	}
	return event
}

func (list *InputList[T]) SetFocus(index int) {
	list.FocusIndex = index
	list.App.SetFocus(list.Children[list.FocusIndex].Primitive())
}

func (list *InputList[T]) AddChild(child T) {
	index := len(list.Children)
	child.SetFocusFn((func() func() {
		return func() {
			list.FocusIndex = index
		}
	})())
	list.Children = append(list.Children, child)
	list.RecalculateGrid()
	list.Grid.AddItem(child.Primitive(), len(list.Children)-1, 1, 1, 1, 0, 0, false)
}

func (list *InputList[T]) RerenderGrid() {
	list.Grid.Clear()
	list.RecalculateGrid()

	for i, child := range list.Children {
		child.SetFocusFn((func(index int) func() {
			return func() {
				list.FocusIndex = index
			}
		})(i))
		list.Grid.AddItem(child.Primitive(), i, 1, 1, 1, 0, 0, false)
	}
}

func (list *InputList[T]) RecalculateGrid() {
	numRows := len(list.Children)
	if list.MaxHeight > len(list.Children) {
		numRows = list.MaxHeight
	}

	rows := make([]int, numRows)
	//childrenHeight := 0
	for i := 0; i < len(list.Children); i++ {
		rows[i] = list.Children[i].Height()
		//childrenHeight += rows[i]
	}

	if len(list.Children) < numRows {
		for i := len(list.Children); i < numRows; i++ {
			rows[i] = list.MinRowHeight
		}
	}

	/*
		_, _, _, height := list.Grid.GetRect()
		if childrenHeight < height {
			rows = append(rows, height-childrenHeight)
		}
	*/
	list.Grid.SetRows(rows...)
}
