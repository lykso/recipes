package lib

import (
	"errors"
	"fmt"
	"log"
	"os"

	"codeberg.org/lykso/nutrition"
	"gopkg.in/yaml.v3"
	"gorm.io/gorm"
)

type Recipe struct {
	ID              uint `yaml:"cache_id"`
	Title           string
	Author          string
	License         string
	Image           string
	ImageCredit     string `yaml:"image_credit"`
	ImageLicense    string `yaml:"image_license"`
	Servings        uint64
	Tags            []string
	Ingredients     []string
	Directions      []string
	NutrientSources []NutrientSource `yaml:"nutrient_sources"`
	nutritionData   *nutrition.NutritionData
}

type NutrientSource struct {
	Ingredient string
	Quantity   float64
	Unit       string
}

func RecipeFromFile(path string) (*Recipe, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	recipe := Recipe{}

	err = yaml.Unmarshal(data, &recipe)
	if err != nil {
		return nil, err
	}
	return &recipe, nil
}

func (r *Recipe) ToFile(path string) error {
	data, err := yaml.Marshal(r)
	if err != nil {
		return err
	}
	return os.WriteFile(path, data, 0644)
}

func (r *Recipe) Analyze(db *gorm.DB) (*nutrition.NutritionData, error) {
	out := nutrition.NutritionData{ID: r.ID, Name: r.Title, Recipe: true}

	for _, ns := range r.NutrientSources {
		data := nutrition.NutritionData{}
		unit := nutrition.Unit{}
		result := db.Where("name = ?", ns.Ingredient).Take(&data)
		if result.Error != nil {
			return nil, errors.New(fmt.Sprintf("Unknown ingredient in recipe \"%s\": %s", r.Title, ns.Ingredient))
		}

		result = db.Where("unit = ? and nutrition_data_id = ?", ns.Unit, data.ID).Take(&unit)
		if result.Error != nil {
			return nil, errors.New(fmt.Sprintf("Unknown unit in recipe \"%s\" for ingredient \"%s\": %s", r.Title, ns.Ingredient, ns.Unit))
		}
		out = out.Add(data.Multiply((unit.PercentOfNutritionData * ns.Quantity) / 100.0))
	}

	return &out, nil
}

func (r *Recipe) ClearNutritionData() {
	r.nutritionData = nil
}

func (r *Recipe) SaveToDB(db *gorm.DB) error {
	r.ClearNutritionData()
	nd, err := r.Analyze(db)
	if err != nil {
		return err
	}

	existingND := nutrition.NutritionData{}
	result := db.Where("name = ?", r.Title).First(&existingND)
	if result.Error == nil && result.RowsAffected > 0 {
		nd.ID = existingND.ID
	}

	nd.Tags = make([]nutrition.Tag, len(r.Tags))
	for i, tag := range r.Tags {
		nd.Tags[i] = nutrition.Tag{NutritionDataID: nd.ID, Name: tag}
	}

	db.Transaction(func(tx *gorm.DB) error {
		tx.Where("nutrition_data_id = ?", nd.ID).Delete(nutrition.Tag{})

		result = tx.Save(nd)
		if result.Error != nil {
			return result.Error
		}
		return nil
	})

	if nd.ID != 0 {
		r.ID = nd.ID
	}

	return nil
}

func (r *Recipe) NutritionData(db *gorm.DB) *nutrition.NutritionData {
	if r.nutritionData != nil {
		return r.nutritionData
	}

	var err error
	r.nutritionData, err = r.Analyze(db)
	if err != nil {
		log.Fatal(err)
	}

	return r.nutritionData
}
