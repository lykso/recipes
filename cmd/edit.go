package cmd

import (
	"log"

	"codeberg.org/lykso/recipes/lib"
	"github.com/spf13/cobra"
)

// editCmd represents the edit command
var editCmd = &cobra.Command{
	Use:   "edit",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		recipe, err := lib.RecipeFromFile(args[0])
		if err != nil {
			log.Fatal(err)
		}

		db, err := lib.DB(dbPath)
		if err != nil {
			log.Fatal(err)
		}

		tui := lib.NewTUI(db)
		tui.LoadRecipe(recipe)
		tui.FilePath = args[0]
		tui.App.Run()
	},
}

func init() {
	editCmd.PersistentFlags().StringVarP(&dbPath, "database", "d", "", "Path to nutrition.db")
	editCmd.MarkFlagRequired("database")

	rootCmd.AddCommand(editCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// editCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// editCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
