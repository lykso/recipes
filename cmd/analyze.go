package cmd

import (
	"fmt"
	"log"
	"sort"

	"codeberg.org/lykso/nutrition"
	"codeberg.org/lykso/recipes/lib"
	"github.com/spf13/cobra"
)

// analyzeCmd represents the analyze command
var analyzeCmd = &cobra.Command{
	Use:   "analyze",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		recipe, err := lib.RecipeFromFile(args[0])
		if err != nil {
			log.Fatal(err)
		}

		db, err := lib.DB(dbPath)
		if err != nil {
			log.Fatal(err)
		}

		nd, err := recipe.Analyze(db)
		if err != nil {
			log.Fatal(err)
		}

		perServing, _ := cmd.Flags().GetBool("serving")
		showZeroes, _ := cmd.Flags().GetBool("all")
		lines := []string{}
		for k, v := range nd.NutrientMap() {
			if perServing {
				v = v / float64(recipe.Servings)
			}

			if v > 0.009 || showZeroes {
				label, ok := nutrition.NutrientLabel(k)
				if !ok {
					log.Fatal("Encountered a nutrient without a label: " + k)
				}
				lines = append(lines, fmt.Sprintf("%s: %.2f", label, v))
			}
		}

		sort.Strings(lines)
		for _, line := range lines {
			fmt.Println(line)
		}
	},
}

func init() {
	rootCmd.AddCommand(analyzeCmd)
	//xdgDBPath, err := xdg.DataFile("lykso/nutrition/nutrition.db")
	//analyzeCmd.PersistentFlags().StringVarP(&dbPath, "database", "d", xdgDBPath, "Path to nutrition.db")
	analyzeCmd.PersistentFlags().StringVarP(&dbPath, "database", "d", "", "Path to nutrition.db")
	analyzeCmd.MarkFlagRequired("database")

	analyzeCmd.Flags().Bool("all", false, "Include nutrients whose amounts evaluate to < 0.009.")
	analyzeCmd.Flags().Bool("serving", false, "Show per-serving nutrition data instead of total.")

	/*
		if err != nil {
			analyzeCmd.MarkFlagRequired("database")

		} else if _, err = os.Stat(xdgDBPath); err != nil {
			analyzeCmd.MarkFlagRequired("database")
		}
	*/

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// analyzeCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// analyzeCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
