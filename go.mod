module codeberg.org/lykso/recipes

go 1.20

replace codeberg.org/lykso/nutrition v0.0.0 => ../nutrition

require (
	codeberg.org/lykso/nutrition v0.0.0
	github.com/glebarez/sqlite v1.8.0
	github.com/rivo/tview v0.0.0-20230511053024-822bd067b165
	github.com/spf13/cobra v1.7.0
	gopkg.in/yaml.v3 v3.0.1
	gorm.io/gorm v1.25.1
)

require (
	github.com/adrg/xdg v0.4.0 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell/v2 v2.6.0 // indirect
	github.com/glebarez/go-sqlite v1.21.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/iancoleman/strcase v0.2.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/term v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	gorm.io/driver/sqlite v1.4.4 // indirect
	modernc.org/libc v1.22.3 // indirect
	modernc.org/mathutil v1.5.0 // indirect
	modernc.org/memory v1.5.0 // indirect
	modernc.org/sqlite v1.21.1 // indirect
)
